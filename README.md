#### For details please check [DESAFIO CREDIT SUISSE.pdf](DESAFIO CREDIT SUISSE.pdf)

### Question 1:
#### Write a C# console application using object oriented design that classifies all trades in a given portfolio. Keep in mind that the real application may have dozens of categories, so your design must be extensible allowing those categories to be easily added/removed/modified in the future.

> Check this repo

### Question 2:
#### A new category was created called PEP (politically exposed person). Also, a new bool property IsPoliticallyExposed was created in the ITrade interface. A trade shall be categorized as PEP if IsPoliticallyExposed is true. Describe in at most 1 paragraph what you must do in your design to account for this new category

> Add `IsPoliticallyExposed` to ITrade; Add a new implementation of `ITradeCategorizer` and add a concrete instance of this new implementation to the list of `categorizers`, as shown below.

> [Program.cs](Credit Suisse/Credit Suisse/Program.cs)
```csharp
List<ITradeCategorizer> categorizers = new List<ITradeCategorizer>() {
    new ExpiredTradeCategorizer(30, "EXPIRED"),
    new HighRiskTradeCategorizer(1000000, "HIGHRISK"),
    new MediumRiskTradeCategorizer(1000000, "MEDIUMRISK"),

    new PoliticallyExposedTradeCategorizer(), // new concrete instance here, reorder if different precedence

    new UncategorizedTradeCategorizer(),
};
```