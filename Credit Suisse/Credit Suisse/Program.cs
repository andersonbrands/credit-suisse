﻿using System;
using System.Collections.Generic;
using Credit_Suisse.Core;
using Credit_Suisse.Core.TradeCategorizers;


namespace Credit_Suisse {
    static class Program {
        static void Main(string[] args) {
            var (trades, refDate) = ReadFromInput();

            foreach (var category in CategorizeTrades(trades, refDate)) {
                Console.WriteLine(category);
            }
        }

        static Tuple<List<ITrade>, DateTime> ReadFromInput() {
            string refDateString = Console.ReadLine().Trim();
            DateTime refDate = DateTime.ParseExact(refDateString, "MM/dd/yyyy", null);

            int tradesCount = Convert.ToInt32(Console.ReadLine().Trim());

            List<ITrade> trades = new List<ITrade>();

            var factory = new TradeFactory();

            for (int i = 0; i < tradesCount; i++) {
                string[] splited = Console.ReadLine().Split();
                var trade = factory.Create(
                    Convert.ToDouble(splited[0]),
                    splited[1],
                    splited[2]
                );
                trades.Add(trade);
            }

            return Tuple.Create(trades, refDate);
        }

        static List<string> CategorizeTrades(List<ITrade> trades, DateTime refDate) {
            List<ITradeCategorizer> categorizers = new List<ITradeCategorizer>() {
                new ExpiredTradeCategorizer(30, "EXPIRED"),
                new HighRiskTradeCategorizer(1000000, "HIGHRISK"),
                new MediumRiskTradeCategorizer(1000000, "MEDIUMRISK"),
                new UncategorizedTradeCategorizer(),
            };

            List<string> categories = new List<string>();

            foreach (var trade in trades) {
                foreach (var categorizer in categorizers) {
                    var category = categorizer.Categorize(trade, refDate);
                    if (category != null) {
                        categories.Add(category);
                        break;
                    }
                }
            }

            return categories;
        }

    }
}
