﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core {
    public class Trade : ITrade {
        public double Value { get; }
        public string ClientSector { get; }
        public DateTime NextPaymentDate { get; }

        public Trade(double value, string sector, DateTime nextPaymentDate) {
            Value = value;
            ClientSector = sector;
            NextPaymentDate = nextPaymentDate;
        }
    }
}
