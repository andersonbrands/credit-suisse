﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core {
    class TradeFactory {

        public ITrade Create(double value, string sector, string nextPaymentDate) {
            var dateTime = DateTime.ParseExact(nextPaymentDate, "MM/dd/yyyy", null);
            return new Trade(value, sector, dateTime);
        }

    }

}
