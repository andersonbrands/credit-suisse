﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core.TradeCategorizers {
    public class ExpiredTradeCategorizer : ITradeCategorizer {
        private readonly int daysToExpire;
        public string Category { get; }

        public ExpiredTradeCategorizer(int daysToExpire, string category) {
            this.daysToExpire = daysToExpire;
            Category = category;
        }

        public string Categorize(ITrade trade, DateTime refDate) {
            bool isLate = (trade.NextPaymentDate < refDate);
            bool isDaysToExpireLate = (refDate - trade.NextPaymentDate).Days > daysToExpire;

            if (isLate && isDaysToExpireLate) {
                return Category;
            } else {
                return null;
            }
        }
    }
}
