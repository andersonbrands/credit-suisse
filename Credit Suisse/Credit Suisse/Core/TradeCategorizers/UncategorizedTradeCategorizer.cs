﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core.TradeCategorizers {
    public class UncategorizedTradeCategorizer : ITradeCategorizer {
        public string Category { get { return "UNCATEGORIZED"; } }

        public string Categorize(ITrade trade, DateTime refDate) {
            return Category;
        }
    }
}
