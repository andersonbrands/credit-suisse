﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core.TradeCategorizers {
    public class HighRiskTradeCategorizer : ITradeCategorizer {

        public string Category { get; }
        private readonly double valueRef;

        public HighRiskTradeCategorizer(double valueRef, string category) {
            this.valueRef = valueRef;
            Category = category;
        }

        public string Categorize(ITrade trade, DateTime refDate) {
            bool greaterThanValueRef = trade.Value > valueRef;
            bool isPrivateSector = trade.ClientSector == "Private";
            if (greaterThanValueRef && isPrivateSector) {
                return Category;
            } else {
                return null;
            }
        }
    }
}
