﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core.TradeCategorizers {
    public class MediumRiskTradeCategorizer : ITradeCategorizer {
        public string Category { get; }
        private readonly double valueRef;

        public MediumRiskTradeCategorizer(double valueRef, string category) {
            this.valueRef = valueRef;
            Category = category;
        }

        public string Categorize(ITrade trade, DateTime refDate) {
            bool greaterThanValueRef = trade.Value > valueRef;
            bool isPublicSector = trade.ClientSector == "Public";
            if (greaterThanValueRef && isPublicSector) {
                return Category;
            } else {
                return null;
            }
        }

    }
}
