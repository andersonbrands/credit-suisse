﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core {
    public interface ITrade {
        double Value { get; }
        string ClientSector { get; }
        DateTime NextPaymentDate { get; }

    }
}
