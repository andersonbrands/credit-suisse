﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Credit_Suisse.Core {
    interface ITradeCategorizer {
        string Category { get; }
        string Categorize(ITrade trade, DateTime refDate);

    }
}
